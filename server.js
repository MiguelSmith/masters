/*
    server for a spatial publish/subscribe system
*/
/*
    functions:
    on()                    handles client connection
    subscribe()             subscribe to an area
    unsubscribe()           unsubscribe from an area
    publish()               send a message to subscribers whose AoI intersects the clients
    move()                  change the coordinates and AoI of the client
    ID()                    return the client id to the client on connnection
*/
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var client_id = -1;

//map of connected clients
var client_list = [];

//map of subscribers for a channel
var subscribers = [];

//map of channels of subscribers
var channels = [
    [],
    []
];

//temporary socket -> ID map
var IDs = new Map();

//serve html file
app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

//handle client connnection
io.on('connection', function(socket){
    client_id++;

    //populate client list
    var arr = [
        socket,                                     //0 - socket of connection between client and server
        Math.floor((Math.random()*300)-150),        //1 - x coordinate of client
        Math.floor((Math.random()*300)-150),        //2 - y coordinate of client
        0,                                          //3 - AOI
        client_id                                   //4 - client ID
    ]
    client_list[client_id] = arr;

    //map ID to socket for disconnect
    IDs.set(socket,client_id);

    //return ID and client list to client
    socket.emit('clients', clientList_to_string(client_id));

    //emit message to all clients that client has joined
    io.emit('client joined', client_to_string(client_id));

    //handle a disconnect
    socket.on('disconnect', function(id){
        var id = IDs.get(socket);
        delete client_list[id];
        delete subscribers[id];
        io.emit('disconnect', id);
        console.log('user disconnected with an id of:' + id);

        return false;
    });

    //publish to subscribers in an area
    socket.on('publish-point', function(msg){
        console.log(msg);
        var temp = msg.split(';');

        var msg = temp[0]+';'+temp[4]+';'+temp[1]+';'+temp[2];

        console.log('Client ' + temp[0]+' said: "' + temp[4] + '" at ('+temp[1]+','+temp[2]+') in a radius of '+temp[3]);

        //loop through subscribers and send message if within range
        for (var keys in subscribers) {
            var dx = Math.pow(parseInt(temp[1])-subscribers[keys][0],2);
            var dy = Math.pow(parseInt(temp[2])-subscribers[keys][1],2);
            var radius = Math.sqrt(dx+dy);

            //check whether AoIs intersect
            if (radius <= subscribers[keys][2]){
                client_list[keys][0].emit('publish', msg);
            };
        };

        return false;
    });

    //publish to subscribers
    socket.on('publish-area', function(msg){
        var temp = msg.split(';');
        var msg = temp[0]+';'+temp[4]+';'+temp[1]+';'+temp[2];

        //loop through subscribers and send message if within range
        for (var keys in subscribers) {
            var dx = Math.pow(parseInt(temp[1])-subscribers[keys][0],2);
            var dy = Math.pow(parseInt(temp[2])-subscribers[keys][1],2);
            var radius = Math.sqrt(dx+dy);
            var test = parseInt(temp[3]) + parseInt(subscribers[keys][2]);

            //check whether AoIs intersect
            if (radius <= test){
                client_list[keys][0].emit('publish', msg);
            };
        };

        return false;
    });

    //handle a subscribe or unsubscribe function
    socket.on('function', function(msg){
        var temp = msg.split(';');
        var func = temp[1].slice(1,temp[1].length);

        /*var func = temp[1].slice(1,temp[1].search(" "));
        var channel = temp[1].slice(temp[1].search(" "), temp[1].length);*/
        var ref = parseInt(temp[0]);

        switch (func) {
            //subscribe
            case 'join': {
                /*switch (channel) {
                    case 'global': {

                    }
                        break;

                    case 'local': {

                    }
                        break;
                    default:
                        console.log("Channel creation not yet supported");
                }*/

                var arr = [client_list[ref][1],client_list[ref][2],client_list[ref][3]];
                subscribers[ref] = arr;
                console.log('Client '+ref+ ' subscribed to the area!');
                //console.log('Subscribers after sub: '+subscribers);
                socket.emit('subscribe', 'You are now subscribed to receive messages');
            }
            break;

            //unsubscribe
            case 'unsub': {
                delete subscribers[ref];
                console.log('Client '+temp[0]+' unsubscribed from the area');
                client_list[temp[0]][0].emit('unsubscribe', 'You have unsubscribed!');
            }
            break;

            //unknown command
            default: {
                console.log('Cannot understand function');
                client_list[temp[0]][0].emit('unknown', 'Unsupported function');
            }
            break;
        }

        return false;
    });

    //move
    socket.on('move', function(msg){
        //handle input message
        var temp = msg.split(';');
        var ref = parseInt(temp[0]);
        console.log('Client '+temp[0]+ ' changed their (x,y) position to <'+temp[1]+','+temp[2]+'> with an AoI of '+temp[3]);

        //insert x, y and Aoi into 2D array
        client_list[temp[0]][1] = temp[1];
        client_list[temp[0]][2] = temp[2];
        client_list[temp[0]][3] = temp[3];

        //update subscriber list if subscribed
        if (subscribers[temp[0]] != undefined){
            subscribers[temp[0]][0] = temp[1];
            subscribers[temp[0]][1] = temp[2];
            subscribers[temp[0]][2] = temp[3];
        }

        //create output message
        temp = ref+" "+temp[1]+' '+temp[2]+" "+temp[3];
        io.emit('move', temp);

        return false;
    });

    return false;
});

var clientList_to_string = function(clientID) {
    var string = clientID+" "+client_list[clientID][1]+" "+client_list[clientID][2]+" "+client_list[clientID][3];
    for (var keys in client_list) {
        string += " "+client_list[keys][1]+","+client_list[keys][2]+","+client_list[keys][3]+","+keys;
    }
    return(string);
}

var client_to_string = function(clientID) {
    var string = clientID+" "+client_list[clientID][1]+" "+client_list[clientID][2]+" "+client_list[clientID][3];
    return string;
}

//set the server to listening
http.listen(3000, function(){

  console.log('listening on localhost:3000');
});
